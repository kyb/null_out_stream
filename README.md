# null_out_stream
Missed std::cnul, `/dev/null` in C++

`null_out_stream` is the same thing as well-known `/dev/null` but 
it is C++ iostream  like `std::cout`;

Use it to discard some data.

## My usage case
In bash we express:
```
/bin/program 2>1 >/dev/null
```
to suppress outputs of the called program.

In C++ it looks like a function call:
```C++
int run_proc(const char* progname, ostream stdin, istream stdout, istream stderr );
// [io]stream could be anything corresponding to std::cout/std::cin, like null_out_stream
```
`null_out_stream` stands for linux's `/dev/null` in C++.

### The other way:
```
std::ostream cnul ("/dev/null");
```
But this is platform-dependant, and relies on linux kernel.
